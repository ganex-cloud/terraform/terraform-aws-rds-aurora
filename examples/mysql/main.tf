module "rds-DATABASE-stage" {
  source                          = "git::https://gitlab.com/ganex-cloud/terraform/terraform-aws-rds-aurora.git?ref=master"
  name                            = "DATABASE-stage"
  password                        = "${file(".secrets/rds-DATABASE-stage.txt")}"
  engine                          = "aurora-mysql"
  engine_version                  = "5.7.mysql_aurora.2.07.1"
  db_parameter_group_family       = "aurora-mysql5.7"
  subnets                         = ["subnet-9ce65ad5n", "subnet-83f131d8", "subnet-0df83520"]
  vpc_id                          = "vpc-834370e4"
  replica_count                   = 1
  instance_type                   = "db.t3.small"
  apply_immediately               = true
  enabled_cloudwatch_logs_exports = ["audit", "error", "general", "slowquery"]
  copy_tags_to_snapshot           = true
  deletion_protection             = false
  backup_retention_period         = 7
  preferred_maintenance_window    = "mon:05:30-mon:06:00"
  preferred_backup_window         = "05:00-05:30"
  allowed_security_groups         = ["sg-00dcfa4de7855b0e4", "sg-38a48a47"]

  #tags = {
  #  backup_plan = "${module.backup_plan-DATABASE-stage-01.name}"
  #}
}
